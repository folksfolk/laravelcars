<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Auth')->group(function(){
    Route::post('/register', 'RegisterController');
    Route::post('/login', 'LoginController');
    Route::post('/logout', 'LogoutController');
});

Route::middleware(['auth', 'authMiddleware'])->group(function(){
    Route::get('get_pelanggan', 'PelangganController@index');
    Route:group(['role_id'=>'1'], function(){
        Route::post('pelanggan', 'PelangganController@create');
        Route::put('/pelanggan/{id}', 'PelangganController@update');
        Route::delete('/pelanggan/{id}', 'PelangganController@delete');
    });
});

